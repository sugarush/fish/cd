set -g sugar_on_cd

function @sugar-cd
  if not contains $argv[1] $sugar_on_cd
    set -a sugar_on_cd $argv[1]
  end
end

function cd
  if test -n $argv[1]
    builtin cd $argv[1]
  else
    builtin cd
  end
  for item in $sugar_on_cd
    $item $argv[1]
  end
end
